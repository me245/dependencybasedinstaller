﻿using InstallerCore;
using System;

namespace InstallerCli
{
    class InstallerCli
    {
        static void Main(string[] args)
        {
            Installer installer = new Installer(args);
            if(!installer.IsValidInput)
            {
                Console.WriteLine(installer.ReasonForError());
            }
            else
            {
                Console.WriteLine(installer.InstallOrder());
            }
        }
    }
}
