﻿using NUnit.Framework;
using InstallerCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstallerCore.UnitTests
{
    [TestFixture]
    class InstallerTest
    {
        [Test]
        public void ConstructorShouldAcceptStringArray()
        {
            Installer installer = new Installer(new[] { "1", "2" });
            Assert.IsInstanceOf<Installer>(installer);
        }

        [Test]
        public void ConstructorShouldPopulateIsValidInput()
        {
            Installer installer = new Installer(new[] { "CamelCaser: " });
            Assert.AreEqual(true, installer.IsValidInput);
        }

        [Test]
        public void ConstructorShouldHaveEmptyAsReasonWhenEmtpyInputIsGiven()
        {
            Installer installer = new Installer(new string[] {});
            Assert.AreEqual("Invalid: Empty:  ", installer.ReasonForError());
        }

        [Test]
        public void IsValidInputShouldReturnTrueForInputWithASingleColonSpace()
        {
            Installer installer = new Installer(new[] { "CamelCaser: " });
            Assert.AreEqual(true, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnFalseForInputWithoutASingleColonSpace()
        {
            Installer installer = new Installer(new[] { "CamelCaser" });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnFalseForInputWithMoreThanOneColonSpace()
        {
            Installer installer = new Installer(new[] { "CamelCaser: : " });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnTrueWhenInputContainsAPackageWithNoDependencies()
        {
            Installer installer = new Installer(new[] { "CamelCaser: " });
            Assert.AreEqual(true, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnFalseWhenInputDoesntContainaPackageWithNoDependencies()
        {
            Installer installer = new Installer(new[] { "CamelCaser: LeetMeme" });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnTrueWithAPackageWithADependency()
        {
            Installer installer = new Installer(new[] { "CamelCaser: LeetMeme", "LeetMeme: " });
            Assert.AreEqual(true, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnFalseWithACycleOfTwoPackages()
        {
            Installer installer = new Installer(new[] { "CamelCaser: LeetMeme", "LeetMeme: CamelCaser", "Ice: " });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void IsValidShouldReturnFalseWithPackagesWithPunctuation()
        {
            Installer installer = new Installer(new[] { "CamelCaser: Leet;Meme", "LeetMeme: CamelCaser", "Ice: " });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnFalseWithACycleOfAnyPackages()
        {
            Installer installer = new Installer(new[] { "CamelCaser: LeetMeme", "LeetMeme: FrozenFire", "FrozenFire: CamelCaser", "Ice: " });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void IsValidInputShouldReturnFalseWithADuplicatePackage()
        {
            Installer installer = new Installer(new[] { "CamelCaser: FrozenFire", "CamelCaser: LeetMeme", "LeetMeme: Ice", "FrozenFire: Ice", "Ice: " });
            Assert.AreEqual(false, installer.IsValidInput);
        }

        [Test]
        public void ReasonForErrorShouldReturnMessageSayingIsValidWhenInputIsValid()
        {
            Installer installer = new Installer(new[] { "CamelCaser: LeetMeme", "LeetMeme: " });
            Assert.AreEqual("Input is Valid", installer.ReasonForError());
        }

        [Test]
        public void ReasonForErrorShouldReturnMessageSayingWhyWhenInputHasADuplicate()
        {
            Installer installer = new Installer(new[] { "CamelCaser: FrozenFire", "CamelCaser: LeetMeme", "LeetMeme: Ice", "FrozenFire: Ice", "Ice: " });
            Assert.AreEqual("Invalid: Duplicate Entry: CamelCaser", installer.ReasonForError());
        }

        [Test]
        public void ReasonForErrorShouldReturnMessageSayingWhyWhenInputHasACycle()
        {
            Installer installer = new Installer(new[] { "CamelCaser: LeetMeme", "LeetMeme: FrozenFire", "FrozenFire: CamelCaser", "Ice: " });
            Assert.AreEqual("Invalid: Cycle Detected: CamelCaser, FrozenFire, LeetMeme", installer.ReasonForError());
        }

        [Test]
        public void InstallOrderShouldReturnInvalidInputWhenInputIsInvalid()
        {
            Installer installer = new Installer(new[] { "CamelCaser: FrozenFire", "CamelCaser: LeetMeme", "LeetMeme: Ice", "FrozenFire: Ice", "Ice: " });
            Assert.AreEqual("Input is Invalid", installer.InstallOrder());
        }

        [Test]
        public void InstallOrderShouldReturnInstallOrderWhenInputIsValid()
        {
            Installer installer = new Installer(new[] { "CamelCaser: FrozenFire", "LeetMeme: Ice", "FrozenFire: Ice", "Ice: " });
            Assert.AreEqual("Ice, LeetMeme, FrozenFire, CamelCaser", installer.InstallOrder());
        }

        [Test]
        public void InstallOrderShouldReturnInstallOrderWithSimpleInput()
        {
            Installer installer = new Installer(new[] { "Ice: " });
            Assert.AreEqual("Ice", installer.InstallOrder());
        }
    }
}
