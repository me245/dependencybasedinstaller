﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstallerCore
{
    public class Installer
    {
        private Dictionary<string, string> inputs = new Dictionary<string, string>();
        private List<string> packagesToInstall = new List<string>();
        private readonly string startString = "START#";
        private readonly string invalidString = "INVALID#";

        public Installer(string[] arrayOfPackagesWithDependencies)
        {
            if (arrayOfPackagesWithDependencies.Count() < 1)
            {
                addInvalidWithReason("Empty", " ");
                return;
            }
            foreach (string str in arrayOfPackagesWithDependencies)
            {
                KeyValuePair<string, string> dependencyKeyWithDependentPackageValue =
                    getDependencyandDependentPackage(str);
                addDependencyAndPackageToInputs(dependencyKeyWithDependentPackageValue);
            }
        }

        private void addDependencyAndPackageToInputs(KeyValuePair<string, string> dependencyKeyWithDependentPackageValue)
        {
            string dependency = dependencyKeyWithDependentPackageValue.Key;
            string dependentPackage = dependencyKeyWithDependentPackageValue.Value;
            if(packagesToInstall.Contains(dependentPackage))
            {
                addInvalidWithReason("Duplicate Entry", dependentPackage);
            }
            else
            {
                if (inputs.ContainsKey(dependency))
                {
                    inputs[dependency] = inputs[dependency] + ", " + dependentPackage;
                }
                else
                {
                    inputs.Add(dependency, dependentPackage);
                }
                packagesToInstall.Add(dependentPackage);
            }
        }

        private void addInvalidWithReason(string reason, string dependentPackage)
        {
            if (inputs.ContainsKey(invalidString))
            {
                inputs[invalidString] = inputs[invalidString] + string.Format(", {0}: {1}", reason, dependentPackage);
            }
            else
            {
                inputs.Add(invalidString, string.Format("{0}: {1}", reason, dependentPackage));
            }
        }

        private KeyValuePair<string, string> getDependencyandDependentPackage(string str)
        {
            string dependency;
            string dependentPackage;
            string[] seperator = new string[] { ": " };
            string[] nodes = str.Split(seperator, StringSplitOptions.None);
            if (inputHasInvalidStructure(str, nodes))
            {
                dependency = invalidString;
                dependentPackage = "Invalid Structure: " + string.Join(", ", nodes);
            }
            else
            {
                dependency = nodes[1];
                dependentPackage = nodes[0];
                if (string.IsNullOrEmpty(dependency))
                {
                    dependency = startString;
                }
            }
            return new KeyValuePair<string, string>(dependency, dependentPackage);
        }

        private bool inputHasCycle()
        {
            bool hasCycle = false;
            if(inputs.Count > 2 && inputs.ContainsKey(startString))
            {
                Queue<string> dependentsToCheck = new Queue<string>();
                List<string> packagesChecked = new List<string>();
                dependentsToCheck.Enqueue(inputs[startString]);
                while(dependentsToCheck.Count > 0)
                {
                    string current = dependentsToCheck.Dequeue();
                    if (current.Contains(", "))
                    {
                        string[] split = { ", " };
                        foreach (string package in current.Split(split, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (packagesChecked.Contains(package)) continue;
                            packagesChecked.Add(package);
                            if (inputs.ContainsKey(package))
                            {
                                dependentsToCheck.Enqueue(inputs[package]);
                            }
                        }
                    }
                    else
                    {
                        if (packagesChecked.Contains(current)) continue;
                        packagesChecked.Add(current);
                        if (inputs.ContainsKey(current))
                        {
                            dependentsToCheck.Enqueue(inputs[current]);
                        }
                    }
                }
                if (packagesChecked.Count != packagesToInstall.Count)
                {
                    hasCycle = true;
                    List<string> invalidPackages = new List<string>();
                    StringBuilder sb = new StringBuilder();
                    foreach(string package in packagesToInstall.Except(packagesChecked))
                    {
                        invalidPackages.Add(package);
                    }
                    invalidPackages.Sort();
                    foreach(string str in invalidPackages)
                    {
                        sb.Append(str);
                        if(str != invalidPackages.Last())
                        {
                            sb.Append(", ");
                        }
                    }
                    addInvalidWithReason("Cycle Detected", sb.ToString());
                }
            }
            return hasCycle;
        }

        private bool inputHasInvalidStructure(string str, string[] nodes)
        {
            return nodes.Count() > 2 || !str.Contains(": ") || nodes.Any((pckg) => pckg.Any(char.IsPunctuation));
        }

        private bool inputsContainAPackageWithNoDepencies()
        {
            return inputs.ContainsKey(startString);
        }

        private bool inputsContainInvalidEntries()
        {
            return inputs.ContainsKey(invalidString);
        }

        private bool isValidInput()
        {
            return !inputsContainInvalidEntries() &&
                !inputHasCycle() &&
                inputsContainAPackageWithNoDepencies();
        }

        private StringBuilder installOrder()
        {
            StringBuilder sb = new StringBuilder();
            Queue<string> packages = new Queue<string>();
            packages.Enqueue(inputs[startString]);
            while (packages.Count > 0) 
            {
                string current = packages.Dequeue();
                sb.Append(current + ", ");
                if (current.Contains(", "))
                {
                    string[] split = { ", " };
                    foreach (string package in current.Split(split, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (inputs.ContainsKey(package))
                        {
                            packages.Enqueue(inputs[package]);
                        }
                    }
                }
                else
                {
                    if (inputs.ContainsKey(current))
                    {
                        packages.Enqueue(inputs[current]);
                    }
                }
            }
            int index = sb.ToString().LastIndexOf(',');
            if(index >= 0)
            {
                sb.Remove(index, 2);
            }
            return sb;
        }

        public bool IsValidInput
        {
            get
            {
                return isValidInput();
            }
        }

        public string ReasonForError()
        {
            if (IsValidInput)
            {
                return "Input is Valid";
            }
            else
            {
                return string.Format("Invalid: {0}", inputs[invalidString]);
            }
        }

        public string InstallOrder()
        {
            if(!IsValidInput)
            {
                return "Input is Invalid";
            }
            else
            {
                return installOrder().ToString();
            }
        }
    }
}
